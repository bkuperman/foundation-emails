export var articles = [
    {
        "title": "When to Watch the Royal Wedding in Every Time Zone",
        "excerpt": "The highly anticipated wedding of Prince Harry and Meghan Markle on Saturday will begin at noon London time, meaning that many international fans planning to watch the nuptials will have to schedule accordingly. Royal family devotees will have plenty of ways to tune into the royal wedding come the weekend, with CBS, ABC, BBC America,… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/11/prince-harry-meghan-markle.jpg",
        "tag": "Music",
        "date": "2 minutes ago",
        "author": "Frances Carter"
    },
    {
        "title": "‘Climax’ Director Gaspar Noe Defends On-Screen Male Nudity, Explains Why He Hated ‘Star Wars’ and Walked Out of ‘Black Panther’",
        "excerpt": "Gaspar Noe’s “Climax” has been one of the buzziest — and strangest — films to debut at this year’s Cannes Film Festival. The horror drama follows a group of street dancers, who have a seemingly normal existence until they are all drugged one night in a club. What follows is a bloodbath of psychedelic hallucinations.… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/gaspar-noe-2.jpg",
        "tag": "Music",
        "date": "8 minutes ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "2018-2019 Broadcast Season Producers: Who Scored the Most New Shows?",
        "excerpt": "The 2018-2019 broadcast schedule is officially locked. 36 new shows have been ordered for next season (down slightly from 39 last year), with certain producers and their production banners locking down multiple projects across multiple networks. Greg Berlanti scored three new shows this year, the same number as Aaron Kaplan and Kapital Entertainment. Julie Plec,… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/greg-berlanti-aaron-kaplan-julie-plec.jpg",
        "tag": "Tech",
        "date": "13 minutes ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Diego Céspedes’ ‘Electric Lion’ Wins Cannes Cinéfondation Top Prize",
        "excerpt": "In the Cannes Film Festival’s key film school shorts awards, “El Verano del León Eléctrico” (The Summer of the Electric Lion), by Chile’s Diego Céspedes, a student at the Instituto de Comunicación e Imagen – Universidad de Chile, won the First Jury Prize at 21st Cinéfondation Selection on Thursday. The prize was awarded by a jury… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/electric-lion.jpg",
        "tag": "TV",
        "date": "21 minutes ago",
        "author": "Wanda Reed"
    },
    {
        "title": "Royal Wedding: For Prince Harry and Meghan Markle’s Big Day, Media Orgs Flood Social Media",
        "excerpt": "The British royal wedding of Prince Harry and Meghan Markle — the first American princess in generations — is a global-zeitgeist event made-to-order for social media. For the first time, social media will play an enormous role in how people consume (and react to) every last detail of Harry and Meghan’s historic knot-tying festivities. The… ",
        "image": "https://pmcvariety.files.wordpress.com/2016/02/harry-and-meghan.jpg",
        "tag": "Tech",
        "date": "31 minutes ago",
        "author": "Kevin Jenkins"
    },
    {
        "title": "Showtime Orders ‘City on a Hill’ Starring Kevin Bacon, Aldis Hodge to Series",
        "excerpt": "Showtime has ordered the drama “City on a Hill” to series. The series was created by Chuck MacLean and based on an original idea by Ben Affleck. It is set in the early 1990s in Boston, a time when the city was rife with violent criminals emboldened by local law enforcement agencies in which corruption… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/city-on-a-hill-showtime.jpg",
        "tag": "TV",
        "date": "35 minutes ago",
        "author": "Frances Carter"
    },
    {
        "title": "Black Actresses Raise Cannes Cry Against Racism",
        "excerpt": "Frustrated with the lack of diversity and inclusion in the French film industry, 16 black actresses took to the red carpet in Cannes on Wednesday night, staging a protest against racism just days after 82 women, led by Cannes jury president Cate Blanchett, launched their own call for gender equality. Led by actress Aïssa Maïga (“Bamako”),… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/khadja-nin-france-zobda-aissa-maiga-rachel-khan-nadege-beausson-diagne-marie-philomene-nga-shirley-souagnon.jpg",
        "tag": "Music",
        "date": "41 minutes ago",
        "author": "Susan Martin"
    },
    {
        "title": "‘Call of Duty Black Ops 4’ Kills Single Player Campaign",
        "excerpt": "“Call of Duty Black Ops 4” won’t have a single-player campaign, instead developers Treyarch are betting on three zombies mode, a multiplayer mode and battle royale mode, the company announced Thursday. “It doesn’t have a traditional campaign, instead we’re weaving that into each of the modes,” said Treyarch chairman Mark Lamia. The game was revealed… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/cod.png",
        "tag": "Music",
        "date": "43 minutes ago",
        "author": "Frances Carter"
    },
    {
        "title": "5 Takeaways From the CW’s Upfront",
        "excerpt": "The CW wrapped the 2018 upfronts with its presentation on Thursday at New York City Center. Here are the five key takeaways: 1. Sunday nights aren’t just about Sunday nights. The CW’s addition of a two-hour block of Sunday primetime programming, returning the network to a sixth night, had a significant impact on development and… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/daniel-ezra-taye-diggs.jpg",
        "tag": "Tech",
        "date": "43 minutes ago",
        "author": "Elizabeth Collins"
    },
    {
        "title": "2018-2019 Upfront Studio Scorecard: Volume of New Scripted Series Drops as Biz Faces Big Transitions",
        "excerpt": "The logline on the 2018-2019 network TV development season: A glamorous business in the throes of transition grapples with unseen forces of disruption and the empire-building ambitions of its alpha-executive leaders. The major studios ended upfront week with fewer orders for scripted series from Big Four networks and CW compared to the past few years:… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/05/2017-upfronts-placeholder-spotlight-module-e1526578117650.jpg",
        "tag": "Music",
        "date": "51 minutes ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Cannes: Gaspar Noe’s ‘Climax’ Wins Directors’ Fortnight Top Prize",
        "excerpt": "CANNES — Gaspar Noé’s “Climax,” charting the descent into physical hell of a young dance troupe, won the biggest prize out at Cannes’ 2018 Directors’ Fortnight, its Art Cinema Award. “Lucia’s Grace,” Italian Gianni Zanasi’s woman’s empowerment comedy, snagged the Europa Cinemas Label, awarded to the section’s best European film. Granted by France’s Society of… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/climax.jpg",
        "tag": "Film",
        "date": "1 hour ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Nia Long on Netflix’s ‘Roxanne, Roxanne’ and Returning to TV With ‘NCIS: Los Angeles’",
        "excerpt": "Nia Long’s breakout role came in 1991, at age 20, on “The Fresh Prince of Bel-Air.” Since then, she’s starred in such big-screen comedies as “The Best Man Holiday” and “Big Momma’s House.” She balances her film career, including a starring turn in Netflix’s current “Roxanne, Roxanne,” with her series regular role on “NCIS: Los… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/nia-long.jpg",
        "tag": "Awards",
        "date": "1 hour ago",
        "author": "Susan Martin"
    },
    {
        "title": "Playback: Rob Liefeld on ‘Deadpool 2’ and the Superhero Movie Landscape",
        "excerpt": "Welcome to “Playback,” a Variety / iHeartRadio podcast bringing you exclusive conversations with the talents behind many of today’s hottest films. We’re changing things up this week with comic book legend Rob Liefeld. Liefeld’s creation, Deadpool, exploded onto the superhero scene in his first standalone film in 2016, and the sequel is packed with even… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/rob-liefeld-playback-podcast.jpg",
        "tag": "Awards",
        "date": "1 hour ago",
        "author": "Wanda Reed"
    },
    {
        "title": "An Inside Look At the Life of ‘Empire’ Actress Serayah",
        "excerpt": "Up-and-coming R&B singer and “Empire” actress Serayah and her always-busy schedule is at the center of the camera’s focus in this episode of Variety’s “That’s Life.” The San Diego native moved to Los Angeles with her mother, grandmother and aunt when she was a child. She said she grew up knowing she wanted to be a… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/serayah.jpg",
        "tag": "Film",
        "date": "1 hour ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "‘Mountain’ Filmmakers Scale World’s Heights for Spectacular Footage",
        "excerpt": "Get ready for your heart to skip a few beats if you plan to see the current film “Mountain,” featuring goosebumps-inducing footage of wingsuiters, tightrope walkers, BASE jumpers, skiers, mountain bikers and rock climbers pursuing passions that take them to the world’s highest peaks, from Alaska to Tibet. To make the movie, director Jennifer Peedom… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/mountain-movie.jpg",
        "tag": "Film",
        "date": "1 hour ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "Naya Rivera Lists East Coast Traditional in L.A.’s Los Feliz (EXCLUSIVE)",
        "excerpt": "Less than six months after actress Naya Rivera filed a petition to end her brief and tumultuous marriage to actor Ryan Dorsey, she’s unsurprisingly set their once-shared home in L.A.’s Los Feliz area out for sale at $3.869 million. The People’s Choice, SAG and ALMA award winning “Glee” alum, who most recently appeared in a… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/nayarivera_lf_fi.jpg",
        "tag": "Film",
        "date": "1 hour ago",
        "author": "Michael Taylor"
    },
    {
        "title": "‘Avengers: Infinity War’ Tops ‘Jurassic World’ As Fourth-Biggest Film Globally",
        "excerpt": "Disney-Marvel’s “Avengers: Infinity War” has gone past “Jurassic World” to become the fourth-highest grossing film at the worldwide box office. Disney announced Thursday that the third “Avengers” film hit $1.686 billion worldwide. Domestic grosses reached $562.9 million after 20 days of release, while international totaled $1.123 billion in three weeks. Overseas grosses are led by… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/04/avengers-infinity-war-7.jpg",
        "tag": "Tech",
        "date": "1 hour ago",
        "author": "Michael Taylor"
    },
    {
        "title": "YouTube TV Launches Tastemade, TYT Network 24-Hour Linear Channels",
        "excerpt": "YouTube TV is bringing two digital-first media companies — Tastemade and TYT Network (aka The Young Turks) — into the pay-TV realm. Google’s internet-TV service is adding 24-hour linear channels from Tastemade, a travel, food and lifestyle media company, and TYT, which focuses on news commentary, politics and entertainment. Both will join the YouTube TV… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/youtube-tv-tastemade-young-turks.jpg",
        "tag": "Awards",
        "date": "1 hour ago",
        "author": "Frances Carter"
    },
    {
        "title": "Starz’s ‘The Spanish Princess’ Casts Charlotte Hope, Sets All Female Directors",
        "excerpt": "Charlotte Hope will star in Starz’s “The Spanish Princess,” which is the cabler’s follow-up to “The White Queen” and “The White Princess,” Variety has learned. Hope takes on the titular role, Catherine of Aragon, who is promised in marriage to the future King of England Prince Arthur (played by Angus Imrie). Catherine is described as “the… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/image0021.jpg",
        "tag": "Music",
        "date": "1 hour ago",
        "author": "Gary Morgan"
    },
    {
        "title": "‘American Idol’: How Disney’s Approach to the Winner’s Music Breaks From the Past",
        "excerpt": "ABC’s inaugural “American Idol” season comes to a close on May 21. But for Disney’s powerful marketing machine, to use a term ripped from the hit reality show, the “journey” is just beginning. The singing competition, which studio executives recently renewed amid ratings momentum, is set to launch a promotional assault for the show’s remaining… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/american-idol-final-31.jpg",
        "tag": "Tech",
        "date": "1 hour ago",
        "author": "Gary Morgan"
    },
    {
        "title": "Ubisoft Delays Pirate Game ‘Skull & Bones’ to 2019-2020",
        "excerpt": "Ubisoft is delaying the launch of its upcoming online pirate game “Skull & Bones,” the company said in an earnings report on Thursday. It’s now giving the game a 2019-2020 release window. Ubisoft first revealed the ambitious project at its E3 2017 press conference. It’s a multiplayer action game where players can choose a captain, establish… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/skull-bones.jpg",
        "tag": "Film",
        "date": "1 hour ago",
        "author": "Carol Smith"
    },
    {
        "title": "Lionsgate Taps Nathan Kahane as Motion Picture Group President (EXCLUSIVE)",
        "excerpt": "Lionsgate is overhauling its motion picture business and instituting a new leadership team. Nathan Kahane has been tapped to become president of the motion picture group at Lionsgate, Variety has learned. The move reunites him with Joe Drake, with whom he co-founded Good Universe, the production company behind “Blockers” and “The House.” Drake returned to… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/nathan-kahane-headshot.jpg",
        "tag": "Awards",
        "date": "1 hour ago",
        "author": "Carol Smith"
    },
    {
        "title": "Jordan Peele-Produced Nazi Hunter Drama Ordered to Series at Amazon",
        "excerpt": "Amazon has given a straight-to-series order to a new drama that hails from executive producer Jordan Peele. The series is titled “The Hunt” and follows a diverse band of Nazi Hunters living in 1977 New York City. The Hunters, as they’re known, have discovered that hundreds of high ranking Nazi officials are living among us and… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/02/variety_magazine_oscar_lunch_7901.jpg",
        "tag": "Awards",
        "date": "1 hour ago",
        "author": "Jerry Powell"
    },
    {
        "title": "CW Fall 2018-2019 Trailers: ‘Charmed,’ ‘All American’ (Watch)",
        "excerpt": "The CW has released some of the first trailers for their upcoming fall shows following the release of their 2018-2019 fall schedule on Thursday. “Charmed,” “Legacies,” and “All American” will debut in the fall. “Roswell, New Mexico,” and “In the Dark” will bow at midseason. Watch the trailers and read the official series descriptions below.… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/cmd101c_0494rb.jpg",
        "tag": "Film",
        "date": "2 hours ago",
        "author": "Carol Smith"
    },
    {
        "title": "TV Ratings: Multiple Season Finales Hold Steady",
        "excerpt": "A number of broadcast season finales aired Wednesday night, with most of those holding steady with their prior week ratings. First up, NBC’s “The Blacklist” (0.7 in adults 18-49, 5.1 million viewers) ticked down in the demo but was even in total viewers. On ABC, “The Goldbergs” (1.2, 5.1 million) was steady in the demo… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/01/the-blacklist-100th-episode-2.jpg",
        "tag": "Tech",
        "date": "2 hours ago",
        "author": "Wanda Reed"
    },
    {
        "title": "Cannes: Pathe Intl. Scores Renee Zellweger’s ‘Judy,’ ‘The Wolf’s Call,’ ‘Aladin’ Sequel (EXCLUSIVE)",
        "excerpt": "Pathé International is rolling off a strong Cannes film market with major pre-sales closed on Renee Zellweger starrer “Judy,” submarine action thriller “The Wolf’s Call,” and French comedy “The Brand New Adventures of Aladin.” “Judy,” Rupert Goold’s anticipated film starring the Oscar-winning Zellweger as Judy Garland, has been sold to Japan (Gaga), Australia (EOne), Italy… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/03/judy-first-image-hr-1-cropped.jpg",
        "tag": "Music",
        "date": "2 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Camera Maker RED Brings Holographic Phone to AT&T & Verizon",
        "excerpt": "RED Digital Cinema, best known among Hollywood insiders for its professional high-end cameras, is expanding into the consumer market: The company will start selling a smart phone dubbed the RED Hydrogen One via AT&T and Verizon this summer, both carriers announced Thursday. Little is known about the Hydrogen One at this point, except for one… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/red-hydrogen-one.jpg",
        "tag": "Video",
        "date": "2 hours ago",
        "author": "Elizabeth Collins"
    },
    {
        "title": "Judge Denies CBS’ Request for Temporary Restraining Order Against Shari Redstone’s National Amusements",
        "excerpt": "A Delaware judge has denied CBS’ request for a temporary restraining order to bar controlling shareholder Shari Redstone from blocking a planned vote today by the CBS board of directors on a move that would dilute Redstone’s voting power in the company. Delaware Chancery Court Chancellor Andre Bouchard issued his ruling Thursday just before noon… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/04/cbs-viacom-merger-les-moonves-shari-redstone.png",
        "tag": "Video",
        "date": "2 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Watch the ‘Call of Duty: Black Ops 4’ Reveal Live",
        "excerpt": "The “Call of Duty: Black Ops 4” community reveal event kicks off on Thursday at 1 p.m. ET and you can watch the whole thing unfold in the video below. Expect to hear a lot more about the upcoming game from Treyarch and Activision, including how multiplayer will shape up, the way zombies will be… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/01/call-of-duty-inifinate-war.jpg",
        "tag": "Awards",
        "date": "2 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "‘Cobra Kai’ and YouTube Premium Coming to U.K., Host of European Territories",
        "excerpt": "YouTube’s hit original series “Cobra Kai” is heading to European screens as the video platform preps a raft of launches for its newly-minted Premium service. YouTube’s paid-for service is being rebranded from Red to Premium and will soon launch in the U.K. and across Europe. Premium will be $11.99 a month in the U.S. and current… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/03/cobra-kai-trailer.jpg",
        "tag": "Awards",
        "date": "3 hours ago",
        "author": "Elizabeth Collins"
    },
    {
        "title": "Suzanne Scott Named CEO of Fox News",
        "excerpt": "Veteran programming executive Suzanne Scott was named chief executive of the Fox News, part of the continuing reorganization of 21st Century Fox as it prepares to sell the bulk of its assets to Walt Disney Co. She will report to Lachlan Murdoch and Rupert Murdoch, who will lead the recalibrated parent company. Fox News  already… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/07/sean-hannity-let-there-be-light.jpg",
        "tag": "Tech",
        "date": "3 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Rachel Bloom Reflects on Emotional ‘Crazy Ex-Girlfriend’ Season and Plans for Final Year",
        "excerpt": "Rebecca Bunch (Rachel Bloom) certainly went through the ringer in the third season of CW musical comedy “Crazy Ex-Girlfriend” — which ended on a note that not only saw her biggest breakthrough yet, but also one that will launch the show’s fourth and final season. “We’ve been saying for a long time that we’re writing… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/09/rachel-bloom.jpg",
        "tag": "Tech",
        "date": "3 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "Everything We Know About ’13 Reasons Why’ Season 2",
        "excerpt": "The Season 2 premiere of Netflix’s buzzy teen drama “13 Reasons Why” is fast approaching, and the recently released trailer, featuring the returns of main cast members Katherine Langford, Dylan Minnette, Alisha Boe, and more as the Liberty High students grapple with Hannah’s death, has piqued anticipation for the series’ sophomore run. The new season… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/13rw_209_01249r.jpg",
        "tag": "Video",
        "date": "3 hours ago",
        "author": "Carol Smith"
    },
    {
        "title": "TeamTO Studio Produces Annecy’s Virtual Reality Welcome Video",
        "excerpt": "European CGI Studio TeamTO has produced a Virtual Reality welcome video for the 2018 Annecy Animation Film Festival edition, featuring the star of hit series “Angelo Rules.” Angelo, the toon series’ main character, a 11-year-old sweet-talking genius child with a knack for getting out of trouble, will take festival attendees on an energetic skateboarding romp… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/angelo-annecy.jpg",
        "tag": "Film",
        "date": "3 hours ago",
        "author": "Jerry Powell"
    },
    {
        "title": "Veena Sud on Increasing Asian-American Representation in Hollywood: ‘This Has to Be a Movement’",
        "excerpt": "While women’s movements including #MeToo and Time’s Up grab headlines and Color of Change works designs to empower African-Americans, the Television Academy hosted a discussion on the state of Asian-American representation in Hollywood Wednesday. Although panelists including Jeannie Mai, Kulap Vilaysack, Veena Sud and Howard Meltzer, CSA, and moderator Miguel Santos of Myx TV, acknowledged… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/tv-academy-asian-american-representation-panel_edited.jpg",
        "tag": "Video",
        "date": "3 hours ago",
        "author": "Wanda Reed"
    },
    {
        "title": "New York Music Month Returns in June",
        "excerpt": "New York Music Month will return in June, Mayor Bill de Blasio’s office announced today. Now in its second year, the official celebration of New York City’s diverse music sector features 30 days of events designed to showcase and support the people and venues behind the city’s music scene. More information can be found at NYMusicMonth.nyc.… ",
        "image": "https://pmcvariety.files.wordpress.com/2016/09/new-york-city-skyline.jpg",
        "tag": "Awards",
        "date": "4 hours ago",
        "author": "Jerry Powell"
    },
    {
        "title": "Brad Kern Departs ‘NCIS: New Orleans’ Showrunner Post",
        "excerpt": "Brad Kern has exited his role as showrunner on CBS’ “NCIS: New Orleans,” Variety has confirmed. Kern will no longer serve as executive producer or oversee the procedural drama. He will remain with the series in a consultant capacity. A representative for CBS Television Studios, which produces the series, declined to comment. Variety first reported… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/08/ncis-new-orleans-e1526566772792.jpg",
        "tag": "Film",
        "date": "5 hours ago",
        "author": "Wanda Reed"
    },
    {
        "title": "Cannes: Turkish Sales Company Match Point Scores Sales to Asia (EXCLUSIVE)",
        "excerpt": "Turkish sales company Match Point Entertainment at the Cannes Film Market, has scored Asian sales on four mainstream local titles including action packed “Hurkus: The Legend in the Sky,” ahead of the biopic’s release in Turkey on May 25. A Turkish aviation pioneer, Vecihi Hurkus designed and manufactured the first airplane in Turkey and was… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/ee-anaresim2-1.jpg",
        "tag": "Awards",
        "date": "5 hours ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "CW 2018 Fall Schedule: ‘Jane the Virgin,’ ‘Crazy Ex-Girlfriend,’ ‘iZombie’ to End",
        "excerpt": "Next season will be the last for three CW dramas: “Jane the Virgin,” “Crazy Ex-Girlfriend,” and “iZombie.” The network confirmed Thursday that all three shows will end following 2018-19 as it announced its fall schedule for the upcoming season. “Jane the Virgin” and “iZombie” will end at five seasons and “Crazy Ex-Girlfriend” will end at… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/10/jane-the-virgin1-e1526569529793.jpg",
        "tag": "Video",
        "date": "6 hours ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "Cannes Film Review: ‘Ten Years Thailand’",
        "excerpt": "“Ten Years Thailand” an anthology of shorts by Thai directors Aditya Assarat, Wisit Sasanatieng, Chulayarnnon Siriphol and Apichatpong Weerasethakul, imagines what happens to their country, ruled by a military junta since 2014, a decade from now. Opening with George Orwell’s famous line in “1984”: “Who controls the past controls the future. Who controls the present… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/10-years-thailand.jpg",
        "tag": "Music",
        "date": "6 hours ago",
        "author": "Susan Martin"
    },
    {
        "title": "Stan Lee $1 Billion Lawsuit ‘Preposterous,’ Says POW! Owner Camsing",
        "excerpt": "Camsing International, the Hong Kong company that controls POW! Entertainment, has hit back at claims that Stan Lee was tricked into selling the company he founded. The feted comic book creator, Lee last week announced a $1 billion lawsuit against Camsing in California. Lee claimed that unscrupulous business partners took advantage of his declining eyesight… ",
        "image": "https://pmcvariety.files.wordpress.com/2017/06/rexfeatures_8882774ab.jpg",
        "tag": "Music",
        "date": "7 hours ago",
        "author": "Gary Morgan"
    },
    {
        "title": "Cannes Film Review: ‘The Spy Gone North’ (Gong Jak)",
        "excerpt": "Based on the testimony of infamous South Korean spy “Black Venus,” who once infiltrated the highest ranks of North Korean leadership, Yoon Jong-bin’s [pmc_film_review_snippet]“The Spy Gone North” recounts a tortuous operation that’s more fascinating and far-fetched than many fictional espionage yarns.[/pmc_film_review_snippet] Instead of the usual dose of action and suspense one expects of this genre,… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/the-spy-gone-north-cannes.jpg",
        "tag": "Film",
        "date": "8 hours ago",
        "author": "Frances Carter"
    },
    {
        "title": "Endemol Shine Hires Viacom and Discovery Alum for Germany",
        "excerpt": "Magnus Kastner will take the helm at Endemol Shine Germany. He is veteran of the international business having run Viacom’s Northern Europe unit, Discovery’s German operation, and been president of Dubai-based Rotana. At production and distribution powerhouse Endemol Shine Group he will be chief executive, Germany, reporting to Northern Europe chairman Boudewijn Beusmans. He replaces… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/magnuskastnercberndjaworek-smallres.jpg",
        "tag": "Music",
        "date": "8 hours ago",
        "author": "Michael Taylor"
    },
    {
        "title": "RTL First Quarter Profits Slip, North America Weighs On FremantleMedia",
        "excerpt": "European broadcasting giant RTL has reported a downturn in profit for the first three months of 2018, as has its FremantleMedia production and distribution arm, which blamed a weaker performance at its North America division. “American Idol” producer FremantleMedia, which is searching for a new CEO ahead of Cecile Frot-Coutaz’s departure for YouTube, posted revenue… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/04/149232_5327_edited.jpg",
        "tag": "Awards",
        "date": "8 hours ago",
        "author": "Susan Martin"
    },
    {
        "title": "‘Gangnam Style’ Singer Psy Breaks With YG Talent Agency",
        "excerpt": "South Korean singer-songwriter Psy has left YG Entertainment, the talent agency that helped lift him to global stardom with his hit single “Gangnam Style.” He had been with YG for eight years and had renewed his contract in 2014. One of Korea’s top three agencies, YG announced the news in statement. But it did not… ",
        "image": "https://pmcvariety.files.wordpress.com/2016/05/gangnam-style-psy.jpg",
        "tag": "Video",
        "date": "8 hours ago",
        "author": "Susan Martin"
    },
    {
        "title": "Cannes Film Festival: ‘Sir’",
        "excerpt": "Gently suggestive and mostly confined to richly detailed interiors, the contempo Mumbai-set “Sir” recalls Tran Ahn-hung’s “The Scent of Green Papaya” for its depiction of a furtive love blossoming between an upper-crust architect and his widowed domestic helper. Yet rather than reiterating Tran’s nostalgic fetishization of the docile Asian woman, tyro writer-director Rohena Gera emphasizes… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/119228-1.jpg",
        "tag": "TV",
        "date": "11 hours ago",
        "author": "Sandra Rogers"
    },
    {
        "title": "‘Burning’ Star Steven Yeun Gets Role He’s ‘Been Waiting For’",
        "excerpt": "A breakthrough role on “The Walking Dead” proved Steven Yeun was ready for the spotlight, but the Korean-American star is taking on his meatiest role to date with the Cannes premiere of South Korean auteur Lee Chang-dong’s “Burning.” “I’ve been waiting for this,” says Yeun. A loose adaptation of a short story by Haruki Murakami… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/burning_still-2.jpg",
        "tag": "Tech",
        "date": "12 hours ago",
        "author": "Kevin Jenkins"
    },
    {
        "title": "Cannes Critics’ Week Highlights Morelia Fest’s New Talent Focus",
        "excerpt": "CANNES — Four Cannes 57th Critics’ Week shorts, sourced from the Morelia Festival – “Vuelve a mi ,” “Under the Sun,” “In Deep Water” and “Land of Waters, Sea of Mermaids” – highlighted Wednesday the seemingly bottomless well of young talent emerging in Mexico. In a tradition which runs back to 2005, titles were chosen… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/morelia-short-film-makers.jpg",
        "tag": "Video",
        "date": "12 hours ago",
        "author": "Carol Smith"
    },
    {
        "title": "Cannes Film Review: ‘Burning’",
        "excerpt": "The word “Burning” may just as well describe the smoldering resentment felt by the characters in the beguiling new drama from South Korean master Lee Chang-dong (“Secret Sunshine”) as it does the pyromaniacal acts they perpetrate, taking out their aggression on abandoned greenhouses for lack of a proper outlet for their rage. In contemporary Korea,… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/burning-lee-chang-dong-cannes.jpg",
        "tag": "Music",
        "date": "12 hours ago",
        "author": "Jerry Powell"
    },
    {
        "title": "CMG’s Giulia Prenna Wins 2018 Cinando Awards’ Best Seller Contest",
        "excerpt": "Giulia Prenna, an executive sales and acquisition consultant at Cinema Management Group, won the Best Seller Award at the 3rd Cinando Awards’ Best Seller Contest, which took place May 15-16 at the Cannes Film Market. Prenna was among eight up-and-coming sales executives who pitched from a choice of two first feature projects, Sylvain Robineau’s French… ",
        "image": "https://pmcvariety.files.wordpress.com/2018/05/2-split-copy2.jpg",
        "tag": "Video",
        "date": "14 hours ago",
        "author": "Kevin Jenkins"
    }
];
