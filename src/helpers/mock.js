import {articles} from './data';

var mock = false;
var random = 0;

module.exports = function(type) {
	if(type == 'image'){
		random++;
	}
		switch(type) {
				case 'title':
					return articles[random]["title"];
				case 'image':
					return articles[random]["image"];
				case 'excerpt':
					return truncate(articles[random]["excerpt"], 120);
				case 'author':
					return 'By ' + articles[random]["author"];
				case 'tag':
						return articles[random]["tag"];
		}
  return type;
}

function truncate(text, max)
{
    return text.substr(0,max-1)+(text.length>max?'...':'');
}

function random(){
	return Math.floor(Math.random()*articles.length);
}
